### Dependencies
 - python-numpy
 - python-matplotlib
 - python-pickle


### Usage
 `# python scripts/generate_dataset.py`

 `# python scripts/mlp.py`


### License
Creative Commons    
Attribution-NonCommercial-ShareAlike    
CC BY-NC-SA 4.0    
https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode    


### Acknowledgement
Copyright © Thomio Watanabe    
All rights reserved
