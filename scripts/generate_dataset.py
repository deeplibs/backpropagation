#!/usr/bin/env python
from __future__ import print_function
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy as np
import pickle



# Original linear function
# function y = ax1 + bx2 + c
a = -2
b = 1
c = 4
x1 = np.arange(-3, 7, .25, dtype=float)
x2 = np.arange(-2, 8, .25, dtype=float)
x1, x2 = np.meshgrid(x1, x2)
y = a*x1 + b*x2 + c


# Generate gaussian noise (normal distribution)
mean = 0.0
sigma = 5.0
num_points = y.shape
noise = np.random.normal(mean, sigma, num_points)

# Add noise to output
y2 = y + noise

## Save data
data = np.stack( (y2,x1,x2), axis=0 )
dataset_name = 'dataset.p'
pickle.dump( data, open( dataset_name, "wb" ) )

# Plot data
fig = plt.figure()
ax = Axes3D(fig)
#ax = fig.add_subplot(111, projection='3d')
ax.plot_surface( x1, x2, y, linewidth=0.1, color='k')
ax.scatter(x1, x2, y2, c='b', marker='o')

axis_size = np.arange(-10, 10, .5)
func_label = 'y = ' + str(a) + 'x1 + ' + str(b) + 'x2 + ' + str(c)
print( 'Function = {}*x1 + {}*x2 + {}'.format(a,b,c) )

plt.xlabel('X1')
plt.ylabel('X2')
#plt.zlabel('Y')
#plt.axis([-3, 7, -2, 8])
plt.grid(True)

file_name = 'gaussian_noise.png'
plt.savefig( file_name, format='png' )

plt.show()

