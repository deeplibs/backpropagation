#!/usr/bin/env python
from __future__ import print_function
import numpy as np
import pickle



# Read datset file
file_name = 'dataset.p'
data = pickle.load( open( file_name, 'rb' ) )
data = np.vsplit( data, 3 )
y = data[0]
x0 = data[1]
x1 = data[2]


#  2 input perceptron
#  h0 = w00*h10 + w01*h11 + b00
#  h10 = w100*x0 + w101*x1 + b10
#  h11 = w110*x0 + w111*x1 + b11

# We must adjust w to reduce the loss/error/objective to a local minimum
# Loss funcion: Mean Squared Error (MSE)
# Error = 1/N * sum( (y - h)^2 )
# https://en.wikipedia.org/wiki/Mean_squared_error

# Gradient descent
# We compute the derivetive regarding each weight (w) or bias (b)
# W_Derivative = - 2/N * sum( (y - h)*x )
# where x is the weight input

# Weights initial value = 0
w00 = .01
w01 = .0
b0 = .0

w100 = -.01
w101 = .01
b10 = .01

w110 = .0
w111 = .001
b11 = -.01


# Learning rate
lr = 0.01
num_points = y.size
num_iterations = 200


# Starts iterations from 1
for i in range(1, num_iterations + 1):
    h10 = w100*x0 + w101*x1 + b10
    h11 = w110*x0 + w111*x1 + b11
    h0 = w00*h10 + w01*h11 + b0

    MSE = np.mean( (y - h0)**2 )

    # Partial derivative (gradients) for w and b
    grad0 = -(2./num_points) * (y - h0)
    derivative_w00 = np.sum( grad0 * h10 )
    derivative_w01 = np.sum( grad0 * h11 )
    derivative_b0  = np.sum( grad0 )

    grad10 = w00 * grad0
    derivative_w100 = np.sum(grad10 * x0)
    derivative_w101 = np.sum(grad10 * x1)
    derivative_b10  = np.sum(grad10)

    grad11 = w01 * grad0
    derivative_w110 = np.sum(grad11 * x0)
    derivative_w111 = np.sum(grad11 * x1)
    derivative_b11  = np.sum(grad11)

    # Update weights:
    w00 -= lr * derivative_w00
    w01 -= lr * derivative_w01
    b0  -= lr * derivative_b0

    w100 -= lr * derivative_w100
    w101 -= lr * derivative_w101
    b10  -= lr * derivative_b10

    w110 -= lr * derivative_w110
    w111 -= lr * derivative_w111
    b11  -= lr * derivative_b11


    if i % 5 == 0:
        print( 'iteration =', i, ', MSE =', MSE)

print( 'Final weights, layer 0 = {}, {}, {}'.format(w00, w01, b0) )
print( 'Final weights, layer 1, neuron 0 = {}, {}, {}'.format(w100, w101, b10) )
print( 'Final weights, layer 1, neuron 1 = {}, {}, {}'.format(w110, w111, b11) )

w1 = w00*w100 + w01*w111
w2 = w00*w101 + w01*w110
b = w00*b10 + w01*b11 + b0
print( 'One neuron equivalent weights = {}, {}, {}'.format(w1, w2, b) )

#print( 'y = {}*x1 + {}*x2 + {}'.format(w1, w2, b)  )

